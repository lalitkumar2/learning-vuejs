// var app = new Vue({
//     el:"#app",
//     data:{
//         products:"laptop",
//         discription: "display-15.7inch, 16gb ram, 2tb hard-disk"
//     }
// })

const app = Vue.createApp({
    data(){
        return {
            cart:0,
            product: "socks",
            brand: "Vue Nike",
            image: "./assets/images/socks_blue.jpg",
            // discription:"size 7 material-synthetic leather",
            // url: "https://v3.vuejs.org/guide/introduction.html#getting-started",
            inStock: true,
            // inventory:9,
            details:['50% cotton', '30% wool', '20% polyester'],
            variants:[
                {id: 2234, color: 'green', image: "./assets/images/socks_green.jpg", quantity:40},
                {id: 2235, color: 'blue', image: "./assets/images/socks_blue.jpg", quantity:0},
            ]
        }
    },
    methods:{
        addToCart(){
            this.cart +=1;
        },
        updateImage(variantImage){
            this.image=variantImage;
        },
        decrement(){
            if(this.cart===0){
                return alert("Cart value reaches to zero value");
            }else{
                this.cart -=1;
            }

            
        }
    },
    computed:{
        title(){
            return this.brand
        }
    }
})